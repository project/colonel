module.exports = {
  purge: {
    enabled: process.env.NODE_ENV === 'development' ? false : true,
    mode: 'layers',
    content: [
      './templates/**/*.html.twig',
      '../../modules/custom/**/*.html.twig',
      '../../modules/custom/**/*.js',
      './assets/js/**/*.js',
      './src/**/*.html',
      './src/**/*.js',
    ]
  },
  darkMode: false,
  theme: {
    colors: {
      'body': "#000",
      'primary': "#004494",
      'secondary': "#868E96",
      'white': '#FFF',
      'gray-1': 'rgba(255, 255, 255, 0.5)',
    },
    borderRadius: {
      'none': '0',
      'small': '4px',
      'full': '9999px',
    },
    borderColor: theme => ({
      ...theme('colors'),
      'transparent': 'transparent',
      'primary': '#004494',
      'dark': '#424242',
      'secondary': '#DADADA',
    }),
    backgroundColor: theme => ({
      ...theme('colors'),
      'transparent': 'transparent',
      'page': '#FFF',
      'header': '#000',
      'primary': '#484AC3',
      'primary-variant': '#5759DD',
      'primary-hover': 'rgba(235, 242, 250, 0.15)',
      'secondary': '#FF8E4D',
      'white': '#FFF',
      'gray-1': 'rgba(255, 255, 255, 0.5)',
      'mask': '#000',
    }),
    extend: {
      boxShadow: {
        'card': '0px 0px 12px 0px rgba(128, 122, 122, 0.2)',
      },
    },
  }
}
