# Colonel - Starter Kit Theme using Tailwindcss

# Get started

From the root of this theme's folder:

Install the packages.

```
npm install
```

Minimified for production (important to run that command before any commit):

```
npm run prod
```

Compile once in development mode:

```
npm run dev
```

Watch changes and compile on the fly (to use for development):

```
npm run watch
```

Same than the previous one but with browserSync in addition. With browserSync, there is no need to reload the page after compiling the files. CSS is automatically injected without reloading the page.

```
Open the file package.json and update the string "YOUR_LOCAL_URL_HERE" on line 20 with the address of your local then run:
npm run sync
```

# Note about Tailwindcss

Do not use string concatenation to create Tailwindcss classes.

DO NOT:

```
<div class="text-{{ highlighted ? '5' : '2' }}xl">...</div>
```

DO:

```
<div class="{{ highlighted ? 'text-5xl' : 'text-2xl' }}">...</div>
```

Made by [Inovae](https://www.inovae.ch/)
